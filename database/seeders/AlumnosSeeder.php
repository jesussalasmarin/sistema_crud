<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AlumnosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('alumnos')->insert([
            'nombre' => 'jesus',
            'apellido_paterno' => 'salas',
            'apellido_materno' => 'marin',
            'semestre' => 1,
            'grupo' => 'C',
        ]);
    }
}
